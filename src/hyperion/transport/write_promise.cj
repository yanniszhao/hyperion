/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package hyperion.transport

from std import sync.Monitor

/**
  * 用于注册回调逻辑
  */
public interface WriteActionListener {
    func beforeWrite(): Unit
}

/**
  * 跟踪写数据操作是否成功。
  *
  * 每个请求使用一个单独的WritePromise，不能多请求共享使用。
  */
public class WritePromise {
    private var success: ?Bool = None

    private var exception: ?Exception = None

    private let listeners = ArrayList<WriteActionListener>()

    private let calledBeforeWrite = AtomicBool(false)

    private let monitor = AtomicOptionReference<Monitor>()

    private var awaitCompleteTimeout: Duration = Duration.second * 60

    public func setSuccess(): Unit {
        this.success = true
        if (let Some(monitor) <- monitor.load()) {
            monitor.notifyAll()
        }
    }

    public func setFailure(ex: Exception): Unit {
        this.exception = ex
        this.success = false
        if (let Some(monitor) <- monitor.load()) {
            monitor.notifyAll()
        }
    }

    /**
     * 等待写数据操作完成
     */
    public func awaitComplete() {
        if (let Some(success) <- success) {
            return
        }

        if (let Some(ex) <- exception) {
            return
        }

        let endTime = DateTime.now().toUnixTimeStamp().toMilliseconds() + awaitCompleteTimeout.toMilliseconds()
        var remainingTime = awaitCompleteTimeout.toMilliseconds()
        let monitor = getMonitor()
        synchronized(monitor) {
            while (true) {
                if (let Some(success) <- success) {
                    return
                }

                if (let Some(ex) <- exception) {
                    return
                }

                if (remainingTime <= 0) {
                    break
                }

                monitor.wait(timeout: Duration.millisecond * remainingTime)
                remainingTime = endTime - DateTime.now().toUnixTimeStamp().toMilliseconds()
            }
        }

        throw TransportException("Unable to await WritePromise complete in ${awaitCompleteTimeout}")
    }

    private func getMonitor(): Monitor {
        if (let Some(monitor) <- monitor.load()) {
            return monitor
        } else {
            let created = Monitor()
            if (monitor.compareAndSwap(None, created)) {
                return created
            }

            if (let Some(monitor) <- monitor.load()) {
                return monitor
            }
        }

        throw TransportException("Failure to obtain monitor for WritePromise")
    }

    public func isSuccess(waitComplete!: Bool): Bool {
        if (waitComplete) {
            this.awaitComplete()
        }

        if (let Some(success) <- success) {
            return success
        }

        return false
    }

    public func getFailure(waitComplete!: Bool): ?Exception {
        if (waitComplete) {
            this.awaitComplete()
        }

        if (let Some(ex) <- exception) {
            return ex
        }

        return None
    }

    /*
     * 添加写之前的回调器
     */
    public func addWriteActionListener(listener: WriteActionListener): Unit {
        listeners.append(listener)
    }

    /**
     * 写入之前回调
     */
    public func beforeWrite(): Unit {
        if (listeners.size > 0 && calledBeforeWrite.compareAndSwap(false, true)) {
            // 因每个请求使用一个单独的WritePromise，不能多请求共享使用，应避免重复调用callback
            for (listener in listeners) {
                listener.beforeWrite()
            }
        }
    }
}
